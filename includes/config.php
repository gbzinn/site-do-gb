<?php

/**
 * Used to store website configuration information.
 *
 * @var string or null
 */
function config($key = '')
{
    $config = [
        'name' => 'ROMs dos amigos',
        'site_url' => '/simple-php-website',
        'pretty_uri' => false,
        'nav_menu' => [
            '' => 'Home',
            'about-us' => 'About Us',
            'emulators' => 'Emulators',
            'contact' => 'Contact',
            'roms' => 'Roms',
        ],
        'template_path' => 'template',
        'content_path' => 'content',
        'version' => 'v3.4',
    ];

    return isset($config[$key]) ? $config[$key] : null;
}

